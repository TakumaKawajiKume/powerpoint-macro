Attribute VB_Name = "Module1"
Sub ProgressBar()
On Error Resume Next
With ActivePresentation
Y = 0
For Each Slide In .Slides
If Not Slide.SlideShowTransition.Hidden Then
Y = Y + 1
End If
Next
C = 0
For x = 2 To .Slides.Count
.Slides(x).Shapes("PB").Delete
If Not .Slides(x).SlideShowTransition.Hidden Then
Set s = .Slides(x).Shapes.AddShape(msoShapeRectangle, _
0, .PageSetup.SlideHeight - 6, _
(C + 1) * .PageSetup.SlideWidth / (Y - 1), 6)
s.Fill.ForeColor.RGB = RGB(0, 128, 128)
s.Name = "PB"
C = C + 1
End If
Next x:
End With
End Sub


