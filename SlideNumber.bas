Attribute VB_Name = "Module2"
Sub SlideNumber()
On Error Resume Next
With ActivePresentation
Y = 0
For Each Slide In .Slides
If Not Slide.SlideShowTransition.Hidden Then
Y = Y + 1
End If
Next
C = 0
For x = 2 To .Slides.Count
.Slides(x).Shapes("SN").Delete
If Not .Slides(x).SlideShowTransition.Hidden Then
C = C + 1
Dim oShape As Shape
Set oShape = .Slides(x).Shapes.AddTextbox(msoTextOrientationHorizontal, .PageSetup.SlideWidth - 50, 0, 50, 28)
With oShape
.TextFrame.TextRange.Text = C & "/" & Y - 1
.TextFrame.TextRange.Font.Color = RGB(100, 100, 100)
.TextEffect.FontName = "Helvetica"
.TextEffect.FontSize = 14
.TextEffect.Alignment = msoTextEffectAlignmentRight
oShape.Name = "SN"
End With
End If
Next x:
End With
End Sub
